public void onSensorChanged(int sensor, float[] values) {
    synchronized (this) {
        if (sensor == SensorManager.SENSOR_ORIENTATION) {
            Log.d(TAG, "onSensorChanged: " + sensor + ", x: " + values[0]
                    + ", y: " + values[1] + ", z: " + values[2]);
 
            // OrientText.setText("--- NESW ---");
            if (Math.abs(values[0] - DegressQuondam) < 1)
                return;
 
            switch ((int) values[0]) {
            case 0: // North 北
                OrientText.setText("正北");
                break;
            case 90: // East 东
                OrientText.setText("正东");
                break;
            case 180: // South 南
                OrientText.setText("正南");
                break;
            case 270: // West 西
                OrientText.setText("正西");
                break;
            default: {
                int v = (int) values[0];
                if (v > 0 && v < 90) {
                    OrientText.setText("北偏东" + v);
                }
 
                if (v > 90 && v < 180) {
                    v = 180 - v;
                    OrientText.setText("南偏东" + v);
                }
 
                if (v > 180 && v < 270) {
                    v = v - 180;
                    OrientText.setText("南偏西" + v);
                }
                if (v > 270 && v < 360) {
                    v = 360 - v;
                    OrientText.setText("北偏西" + v);
                }
            }
            }
 
            ((TextView) findViewById(R.id.OrientValue)).setText(String
                    .valueOf(values[0]));
 
            if (DegressQuondam != -values[0])
                AniRotateImage(-values[0]);
        }
 
        // if (sensor == SensorManager.SENSOR_ACCELEROMETER) { // //}
 
    }
}
 
public void onAccuracyChanged(int sensor, int accuracy) {
    Log.d(TAG, "eoe.cn onAccuracyChanged: " + sensor + ", accuracy: " + accuracy);
}
